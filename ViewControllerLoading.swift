//
//  ViewControllerLoading.swift
//  LayoutBT1
//
//  Created by devsenior1 on 29/04/2022.
//

import UIKit
import PaginatedTableView

class ViewControllerLoading: UIViewController {
    var list = [Int]()
    @IBOutlet weak var TableViewLoad: PaginatedTableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        TableViewLoad.paginatedDelegate = self
        TableViewLoad.paginatedDataSource = self
        TableViewLoad.enablePullToRefresh = true
        TableViewLoad.pullToRefreshTitle = NSAttributedString(string: "Pull to Refresh")
        
        TableViewLoad.loadData(refresh: true)
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension ViewControllerLoading: PaginatedTableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func loadMore(_ pageNumber: Int, _ pageSize: Int, onSuccess: ((Bool) -> Void)?, onError: ((Error) -> Void)?) {
        // Call your api here
        // Send true in onSuccess in case new data exists, sending false will disable pagination
        
        // If page number is first, reset the list
        if pageNumber == 1 { self.list = [Int]() }
        
        // else append the data to list
        let startFrom = (self.list.last ?? 0) + 1
        for number in startFrom..<(startFrom + pageSize) {
            self.list.append(number)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            onSuccess?(true)
        }
    }
}
extension ViewControllerLoading: PaginatedTableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LoadTableViewCell", for: indexPath) as? LoadTableViewCell else {
            fatalError("The dequeued cell is not an instance of TableViewCell.")
        }
        cell.label.text = "Cell Number: \(self.list[indexPath.row])"
        cell.imageView!.image = UIImage(named: "dangyeu.png")
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(scrollView.contentOffset.y)
    }
}
extension ViewControllerLoading {
    // For Swipe Actions
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    // To enable swipe, make sure you overide this method
    public func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        if self.list[indexPath.row] % 3 == 0 {
            return .delete
        } else {
            return .none
        }
    }
}
