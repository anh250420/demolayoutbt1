//
//  ViewController.swift
//  LayoutBT1
//
//  Created by devsenior1 on 26/04/2022.
//

import UIKit
import MPSHorizontalMenu

class ViewController: UIViewController {

    @IBOutlet weak var HL5Btn: UIButton!
    @IBOutlet weak var HL4Btn: UIButton!
    @IBOutlet weak var HL3Btn: UIButton!
    @IBOutlet weak var HL2Btn: UIButton!
    @IBOutlet weak var HL1Btn: UIButton!
    @IBOutlet weak var HelloBtn1: UIButton!
    @IBOutlet weak var TBGioHangBtn: UIButton!
    @IBOutlet weak var AddBtn: UIButton!
    @IBOutlet weak var RemoveBtn: UIButton!
    @IBOutlet weak var TbRaisedBtn: UIButton!
    @IBOutlet weak var Hoicham: UIImageView!
    @IBOutlet weak var Text: UITextField!
    @IBOutlet weak var RaisedBtn: UIButton!
    @IBOutlet weak var ViewMesTbBtn: UIButton!
    @IBOutlet weak var FriendsTbBtn: UIButton!
    @IBOutlet weak var EventsTbBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        HL1Btn.layer.cornerRadius = 9
        HL2Btn.layer.cornerRadius = 11
        HL3Btn.layer.cornerRadius = 13
        HL4Btn.layer.cornerRadius = 16
        HL5Btn.layer.cornerRadius = 19
        HelloBtn1.layer.cornerRadius = 14
        TBGioHangBtn.layer.cornerRadius = 17
        TBGioHangBtn.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        TbRaisedBtn.layer.cornerRadius = 10
        TbRaisedBtn.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        RaisedBtn.layer.cornerRadius = 5
        RaisedBtn.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        RaisedBtn.layer.shadowOpacity = 0.3
        RaisedBtn.layer.shadowColor = UIColor.gray.cgColor
        RaisedBtn.layer.shadowOffset = CGSize(width: 0, height: 0)
        ViewMesTbBtn.layer.cornerRadius = 15
        ViewMesTbBtn.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        FriendsTbBtn.layer.cornerRadius = 15
        FriendsTbBtn.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        EventsTbBtn.layer.cornerRadius = 15
        EventsTbBtn.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        AddBtn.layer.cornerRadius = 5
        AddBtn.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        AddBtn.layer.shadowOpacity = 0.3
        AddBtn.layer.shadowColor = UIColor.gray.cgColor
        AddBtn.layer.shadowOffset = CGSize(width: 0, height: 0)
        RemoveBtn.layer.cornerRadius = 5
        RemoveBtn.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        RemoveBtn.layer.shadowOpacity = 0.3
        RemoveBtn.layer.shadowColor = UIColor.gray.cgColor
        RemoveBtn.layer.shadowOffset = CGSize(width: 0, height: 0)
        // Do any additional setup after loading the view.
       
    }


}

